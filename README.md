# General Information

This repository contains the *django project* that runs the [Bits&Bäume-Wiki](https://bubwiki.uber.space/).

It is built upon on the free software [django-wiki](http://www.django-wiki.org/) and contains some additional code for deployment and backup.
