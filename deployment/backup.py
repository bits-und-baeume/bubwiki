import time
import os
import re
import deploymentutils as du


from ipydex import IPS, activate_ips_on_exception

# simplify debugging
activate_ips_on_exception()


"""
This script serves to generate and download a recent backup from the server.

usage:

python3 backup.py
"""


# call this before running the script:
# eval $(ssh-agent); ssh-add -t 10m


# -------------------------- Begin Essential Config section  ------------------------

config = du.get_nearest_config("config.ini")

remote = config("remote")
user = config("user")


# -------------------------- Begin Optional Config section -------------------------
# if you know what you are doing you can adapt these settings to your needs



# directory for deployment files (e.g. database files)
deployment_dir = config("deployment_dir")

app_name = config("app_name")

# name of the directory for the virtual environment:
venv = "hp_env"

target_deployment_path = f"/home/{user}/{deployment_dir}"
remote_backup_path = f"{target_deployment_path}/db_backups"
local_backup_path = os.path.join(du.get_dir_of_this_file(), "..", "..", "")

# -------------------------- End Config section -----------------------

c = du.StateConnection(remote, user=user, target="remote")
c.activate_venv(f"~/{venv}/bin/activate")

c.chdir(target_deployment_path)

c.run('python manage.py savefixtures --backup', target_spec="remote")

c.rsync_download(remote_backup_path, local_backup_path, filters="", target_spec="remote")



final_msg = f"backup script {du.bgreen('done')}."
print(final_msg)

